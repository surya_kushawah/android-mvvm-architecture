package com.framework.mvvm.di.module

import androidx.lifecycle.ViewModelProvider
import com.framework.mvvm.core.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
interface ViewModelModule {
    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}