package com.framework.mvvm.di.component

import android.app.Application
import com.framework.mvvm.FrameworkApp
import com.framework.mvvm.di.module.AppModule
import com.framework.mvvm.di.module.NetworkModule
import com.framework.mvvm.di.module.ViewModelModule
import com.framework.mvvm.ui.main.builder.MainActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        MainActivityBuilder::class
    ]
)
interface AppComponent : AndroidInjector<FrameworkApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun build(): AppComponent
    }

    override fun inject(app: FrameworkApp)
}
