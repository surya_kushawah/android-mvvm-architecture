package com.framework.mvvm.ui.main.builder

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.framework.mvvm.di.ViewModelKey
import com.framework.mvvm.ui.main.MainActivity
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.framework.mvvm.ui.main.MainViewModel as MainViewModel1

@Module
interface MainActivityModule {
    @Binds
    fun providesAppCompatActivity(mainActivity: MainActivity): AppCompatActivity

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel1::class)
    fun bindMainViewModel(
        mainViewModel: MainViewModel1
    ): ViewModel
}