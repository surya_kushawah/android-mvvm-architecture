package com.framework.mvvm.ui.base

interface RetryListener {
    fun retry()
}