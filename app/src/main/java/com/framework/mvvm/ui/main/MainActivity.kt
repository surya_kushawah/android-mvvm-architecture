package com.framework.mvvm.ui.main

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.framework.mvvm.R
import com.framework.mvvm.core.ViewModelFactory
import com.framework.mvvm.ui.base.BaseActivity
import com.framework.mvvm.core.Navigator
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            navigator.navigateToImages()
        }
    }
}
